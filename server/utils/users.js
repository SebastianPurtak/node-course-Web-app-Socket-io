[{
    id: '65',
    name: 'Wal',
    room: 'Cosplay fans'
}]

class Users{
    constructor(){
        this.users = [];
    }

    addUser(id, name, room){
        var user = {id, name, room};
        this.users.push(user);
        return user;
    }

    removeUser(id){
        var user = this.getUser(id);

        if(user){
            this.users = this.users.filter((user) => user.id !== id);
        }

        return user;
        //własne     
        // var users = this.users.filter((user) => user.id !== id);
        // //var namesArray = users.map((user) => user.name);
        // return users;
    }

    getUser(id){
        return this.users.filter((user) => user.id === id)[0];
        //własne
        // var userArray = this.users.filter((user) => user.id === id);
        // var user = userArray[0];

        // return user;
    }

    getUserList(room){
        var users = this.users.filter((user) => user.room === room);
        var namesArray = users.map((user) => user.name);
        return namesArray;
    }
};

module.exports = {Users};

// class Person{
//     constructor (name, age){
//         this.name = name;
//         this.age = age;
//     }
//     getUserDescription() {
//         return `${this.name} is ${this.age} year(s) old.`;
//     }
// };

// var me = new Person('Stave', 35);
// var description = me.getUserDescription();
// console.log(description);