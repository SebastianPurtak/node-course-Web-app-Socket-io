const expect = require('expect');

const {Users} = require('./users');

describe('Users', () => {

    var users;

    beforeEach(() => {
        users = new Users();
        users.users = [{
            id: '1',
            name: 'Mike',
            room: 'Node cours'
        }, {
            id: '2',
            name: 'Jan',
            room: 'React cours'
        }, {
            id: '3',
            name: 'Tom',
            room: 'Node cours'
        }];
    });

    it('should add new user', () => {
        var users = new Users();
        var user = {
            id: '123',
            name: 'Lol',
            room: 'Kaszanka'
        };
        var resUser = users.addUser(user.id, user.name, user.room);


        expect(users.users).toEqual([user]);
    });

    it('should remove a user', () => {
        var userId = '1';
        var user = users.removeUser(userId);

        expect(user.id).toBe(userId);
        expect(users.users.length).toBe(2);

        //własne
        // var testUser = users.users[0];
        // var userList = users.removeUser('1');

        // expect(userList).not.toContain(testUser);
    });

    it('should not remove user', () => {
        var userId = '44';
        var user = users.removeUser(userId);

        expect(user).toBeFalsy();
        expect(users.users.length).toBe(3);


        //własne
        // var userList = users.removeUser('44');
        
        // expect(userList).toMatchObject(users.users);
    });

    it('should find a user', () => {
        var userId = '2';
        var user = users.getUser(userId);

        expect(user.id).toBe(userId);
        
        //własne
        // var testUser = users.users[0];
        // var user = users.getUser('1');
        
        // expect(user).toBe(testUser);
    });

    it('should not find a user', () => {
        var userId = '99';
        var user = users.getUser(userId);

        expect(user).toBeUndefined();

        //własne
        // var user = users.getUser('15');

        // expect(user).toBeFalsy();
    });

    it('should return names for Node cours', () => {
        var userList = users.getUserList('Node cours');


        expect(userList).toEqual(['Mike', 'Tom']);
    });

    it('should return names for React cours', () => {
        var userList = users.getUserList('React cours');

        expect(userList).toEqual(['Jan']);
    });
});