var moment = require('moment');

var generateMessage = (author, text) => {
    return {
        author,
        text,
        createdAt: moment().valueOf()
    };
};

var generateLocationMessage = (author, latitude, longitude) => {
    return {
        author,
        url: `https://www.google.com/maps?g=${latitude},${longitude}`,
        createdAt: moment().valueOf()
    };
};

module.exports = {generateMessage, generateLocationMessage};