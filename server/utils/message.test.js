var expect = require('expect');

var {generateMessage, generateLocationMessage} = require('./message')

describe('generateMessage', () => {

    it('should generata correct message object', () => {
        var author = 'Jen';
        var text = 'Some message';
        var message = generateMessage(author, text);

        expect(typeof message.createdAt).toBe('number');
        expect(message).toMatchObject({author, text});
    });

    //własne
    // it('should generata correct message object', () => {
    //     var testingObject = generateMessage('XYZ', 'Test message 1');
    //     expect(testingObject.author).toBe('XYZ');
    //     expect(testingObject.text).toBe('Test message 1');
    //     expect(typeof testingObject.createdAt).toBe('number');
    // });

});

describe('generateLocationMessage', () => {
    it('should generate correct location object', () => {

        //własne
        var author = 'TestCase';
        var latitude = '51.165654'; 
        var longitude = '56,616468'; 
        var locationUrl = `https://www.google.com/maps?g=${latitude},${longitude}`;

        var locationMessage = generateLocationMessage(author, latitude, longitude);
        
        expect(locationMessage.author).toBe(author);
        expect(typeof locationMessage.createdAt).toBe('number');
        expect(locationMessage.url).toBe(locationUrl);
    });
});