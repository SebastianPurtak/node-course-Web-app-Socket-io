const expect = require('expect')

//własne
const {isRealString} = require('./validation');

describe('isRealString', () => {
    it('should reject non-string values', () => {
        var number = 15;
        expect(isRealString(number)).toBeFalsy();
    });

    it('should rejesc string with only spaces', () => {
        var testString = '   ';
        expect(isRealString(testString)).toBeFalsy();
    });

    it('should allow string with non-space characters', () => {
        var testString = 'test string';
        expect(isRealString(testString)).toBeTruthy();
    });
});